package com.automationanywhere.botcommand.samples.commands;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class PivotTableCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(PivotTableCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    PivotTable command = new PivotTable();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("filePath") && parameters.get("filePath") != null && parameters.get("filePath").get() != null) {
      convertedParameters.put("filePath", parameters.get("filePath").get());
      if(!(convertedParameters.get("filePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","filePath", "String", parameters.get("filePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("filePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","filePath"));
    }

    if(parameters.containsKey("sheetName") && parameters.get("sheetName") != null && parameters.get("sheetName").get() != null) {
      convertedParameters.put("sheetName", parameters.get("sheetName").get());
      if(!(convertedParameters.get("sheetName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sheetName", "String", parameters.get("sheetName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sheetName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sheetName"));
    }

    if(parameters.containsKey("areaReference") && parameters.get("areaReference") != null && parameters.get("areaReference").get() != null) {
      convertedParameters.put("areaReference", parameters.get("areaReference").get());
      if(!(convertedParameters.get("areaReference") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","areaReference", "String", parameters.get("areaReference").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("areaReference") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","areaReference"));
    }

    if(parameters.containsKey("pivotTableSheetName") && parameters.get("pivotTableSheetName") != null && parameters.get("pivotTableSheetName").get() != null) {
      convertedParameters.put("pivotTableSheetName", parameters.get("pivotTableSheetName").get());
      if(!(convertedParameters.get("pivotTableSheetName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","pivotTableSheetName", "String", parameters.get("pivotTableSheetName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("pivotTableSheetName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","pivotTableSheetName"));
    }

    if(parameters.containsKey("rows") && parameters.get("rows") != null && parameters.get("rows").get() != null) {
      convertedParameters.put("rows", parameters.get("rows").get());
      if(!(convertedParameters.get("rows") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","rows", "String", parameters.get("rows").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("rows") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","rows"));
    }

    if(parameters.containsKey("reportFilter") && parameters.get("reportFilter") != null && parameters.get("reportFilter").get() != null) {
      convertedParameters.put("reportFilter", parameters.get("reportFilter").get());
      if(!(convertedParameters.get("reportFilter") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","reportFilter", "String", parameters.get("reportFilter").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("sumRow") && parameters.get("sumRow") != null && parameters.get("sumRow").get() != null) {
      convertedParameters.put("sumRow", parameters.get("sumRow").get());
      if(!(convertedParameters.get("sumRow") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sumRow", "String", parameters.get("sumRow").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sumRow") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sumRow"));
    }

    if(parameters.containsKey("fieldValue") && parameters.get("fieldValue") != null && parameters.get("fieldValue").get() != null) {
      convertedParameters.put("fieldValue", parameters.get("fieldValue").get());
      if(!(convertedParameters.get("fieldValue") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","fieldValue", "String", parameters.get("fieldValue").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("fieldValue") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","fieldValue"));
    }
    if(convertedParameters.get("fieldValue") != null) {
      switch((String)convertedParameters.get("fieldValue")) {
        case "SUM" : {

        } break;
        case "AVERAGE" : {

        } break;
        case "COUNT" : {

        } break;
        case "MAX" : {

        } break;
        case "MIN" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","fieldValue"));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("filePath"),(String)convertedParameters.get("sheetName"),(String)convertedParameters.get("areaReference"),(String)convertedParameters.get("pivotTableSheetName"),(String)convertedParameters.get("rows"),(String)convertedParameters.get("reportFilter"),(String)convertedParameters.get("sumRow"),(String)convertedParameters.get("fieldValue")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
