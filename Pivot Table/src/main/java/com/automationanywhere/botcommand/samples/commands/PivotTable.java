package com.automationanywhere.botcommand.samples.commands;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.DataConsolidateFunction;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFPivotTable;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "Pivot Table", label = "Pivot Table",
        node_label = "Pivot Table", description = "Pivot Table", icon = "pkg.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "Assign Variable to Receive Success/Failure Message", return_type = STRING, return_required = true)

public class PivotTable {


    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");


    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action (
            /*  ////////////////////////////////////////
                //Assign Input Parameters/Variables
                ////////////////////////////////////////
            */
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "Select Excel File Path")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty String filePath,

            @Idx(index = "2", type = TEXT)
            @Pkg(label = "Enter Data Sheet Name (e.g Sheet1)")
            @NotEmpty String sheetName,

            @Idx(index = "3", type = TEXT)
            @Pkg(label = "Enter Data Range (e.g A1:D50)")
            @NotEmpty String areaReference,

            @Idx(index = "4", type = TEXT)
            @Pkg(label = "Define Pivot Table's Sheet Name (e.g Pivot Output)")
            @NotEmpty String pivotTableSheetName,

            @Idx(index = "5", type = TEXT)
            @Pkg(label = "Enter Column Numbers, Seperated by Comma as your Pivot Row Label (e.g 1,2,3)")
            @NotEmpty String rows,

            @Idx(index = "6", type = TEXT)
            @Pkg(label = "Enter Column Number for Report Filter (e.g 1)")
                String reportFilter,

            @Idx(index = "7", type = TEXT)
            @Pkg(label = "Enter Column Number for Summarized Value (e.g 6)")
            @NotEmpty String sumRow,

            @Idx(index = "8", type = SELECT, options = {
                    @Idx.Option(index ="8.1", pkg = @Pkg(label = "Sum", value = "SUM")),
                    @Idx.Option(index ="8.2", pkg = @Pkg(label = "Average", value = "AVERAGE")),
                    @Idx.Option(index ="8.3", pkg = @Pkg(label = "Count", value = "COUNT")),
                    @Idx.Option(index ="8.4", pkg = @Pkg(label = "Max", value = "MAX")),
                    @Idx.Option(index ="8.5", pkg = @Pkg(label = "Min", value = "MIN"))
            })
            @Pkg(label = "Select Function for Summarized Output")
            @NotEmpty String fieldValue

    ) throws Exception

    {
        //Internal validation, to disallow empty strings. No null check needed as we have NotEmpty on firstString.
        if ("".equals(filePath.trim())) {
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "filePath"));
        }

        if ("".equals(rows.trim())) {
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "rows"));
        }

        //Business logic
        //Trim filePath Variables
        filePath = filePath.trim();
        // Assign File Path to a Variable
        FileInputStream fileInputStream =  new FileInputStream(filePath);

        // Create Workbook variable
        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);

        //Trim sheetName Variables
        sheetName = sheetName.trim();
        // Assign Data Sheet name to a Variable
        XSSFSheet sheet = workbook.getSheet(sheetName);

        //Create a new Sheet for Pivot Table
        XSSFSheet pivotSheet = workbook.createSheet(pivotTableSheetName);

        //Trim areaReference Variables
        areaReference = areaReference.trim();
        //Select area for Pivot Table
        AreaReference source = new AreaReference(sheetName+"!"+areaReference, SpreadsheetVersion.EXCEL2007);

        //Set Reference for Pivot Table ==>> Where to start Pivot Table
        CellReference position = new CellReference("A1");

        // Create a pivot table on Separate Sheet
        XSSFPivotTable pivotTable = pivotSheet.createPivotTable(source, position);
         /* ********************************
         //Configure the pivot table
         ********************************** */
         //Remove white spaces from Rows Variable
        rows = rows.replaceAll("\\s", "");

         //Split String into List Array (Column1, Column2, Column3 etc)
        List<String> iRows = Arrays.asList(rows.split(","));
        System.out.println(iRows);

        int count = 0;
        while (iRows.size() > count) {

            //Covert String into Integer
            int iRowNumber = Integer.parseInt(iRows.get(count));

            //Add Row labels
            pivotTable.addRowLabel(iRowNumber);
            System.out.println(iRowNumber);

            // Set Tabular Format
            pivotTable.getCTPivotTableDefinition().getPivotFields().getPivotFieldArray(iRowNumber).setOutline(false);

            count++;
        }

        //Remove white spaces from sumRow Variable
        sumRow = sumRow.replaceAll("\\s", "");
        //Convert String into Int
        int iSumRow = Integer.parseInt(sumRow);
        //Sum up the second column
        if (fieldValue == "SUM") {
            pivotTable.addColumnLabel(DataConsolidateFunction.SUM, iSumRow);
        } else if (fieldValue == "AVERAGE") {
            pivotTable.addColumnLabel(DataConsolidateFunction.AVERAGE, iSumRow);
        } else if (fieldValue == "COUNT") {
            pivotTable.addColumnLabel(DataConsolidateFunction.COUNT, iSumRow);
        } else if (fieldValue == "MAX") {
            pivotTable.addColumnLabel(DataConsolidateFunction.MAX, iSumRow);
        } else if (fieldValue == "MIN") {
            pivotTable.addColumnLabel(DataConsolidateFunction.MIN, iSumRow);
        }

        //Add Report filter example
        //Covert String into Integer
        if (reportFilter != "") {
            int iReportFilter = Integer.parseInt(reportFilter);
            pivotTable.addReportFilter(iReportFilter);
        }

        FileOutputStream fileOutputStream =  new FileOutputStream(filePath);
        workbook.write(fileOutputStream);
        //workbook.close();

        String result = "Success";

        //Return StringValue.
        return new StringValue(result);
    }

}
